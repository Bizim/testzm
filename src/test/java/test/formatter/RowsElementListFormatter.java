package test.formatter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.*;

public class RowsElementListFormatter extends WebElementsListFormatter {

    protected WebDriver driver;

    public RowsElementListFormatter(WebDriver driver) {
        super(driver);
    }

    /**
     * Получаем длину списка без учета загловка таблицы
     * @param elements Список WebElements
     * @return
     */

    public Integer getLength(List<WebElement> elements) {
        if (elements.size() <= 2) {
            return 0;
        } else {
            return (elements.size() - 2);
        }
    }
}
