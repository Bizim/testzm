package test.formatter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.*;

public class CellsElementListFormatter extends WebElementsListFormatter {

    protected WebDriver driver;

    public CellsElementListFormatter(WebDriver driver) {
        super(driver);
    }

    /**
     * Метод возвращает Map, содержащий значения ячеек страницы. Ключ- xpath ячейки
     * @param elements Список WebElements
     * @return Map<String,String>
     */

    public Map<String, String> getContent(List<WebElement> elements) {
        Map<String, String> formattedStringList = new HashMap<String, String>();

        for (Iterator<WebElement> i = elements.iterator(); i.hasNext(); ) {
            WebElement item = i.next();
            String text = item.getText();
            if (text != null && !text.trim().isEmpty()) {
                String xpath = generateXPATH(item);
                formattedStringList.put(xpath, text.trim());
            }
        }
        return formattedStringList;
    }

}
