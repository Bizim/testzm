package test.formatter;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public abstract class WebElementsListFormatter {

    protected WebDriver driver;

    public WebElementsListFormatter(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Получаем Map, содержащий содержимое каждой ячеки
     * @param elements Список WebElements
     * @return Map<String, String>
     * @throws InterruptedException
     */

    public Map<String, String> getContent(List<WebElement> elements) throws InterruptedException {
        Map<String, String> formattedStringList= new HashMap<String, String>();
        return formattedStringList;
    }

    /**
     * Метод возвращает xpath для WebElement
     * @param element WebElement
     * @return
     */

    public String generateXPATH(WebElement element) {
        return "/html/" + (String)((JavascriptExecutor)driver).executeScript("gPt=function(c){if(c.id!==''){return'id(\"'+c.id+'\")'}if(c===document.body){return c.tagName}var a=0;var e=c.parentNode.childNodes;for(var b=0;b<e.length;b++){var d=e[b];if(d===c){return gPt(c.parentNode)+'/'+c.tagName+'['+(a+1)+']'}if(d.nodeType===1&&d.tagName===c.tagName){a++}}};return gPt(arguments[0]).toLowerCase();", element);
    }

}

