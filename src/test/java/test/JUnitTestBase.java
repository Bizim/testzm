package test;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.ExternalResource;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Capabilities;

import ru.stqa.selenium.factory.WebDriverPool;

import test.util.PropertyLoader;

/**
 * Base class for all the JUnit-based test classes
 */
public class JUnitTestBase {

  protected static String gridHubUrl;
  protected static String baseUrl;
  protected static Capabilities capabilities;

  protected WebDriver driver;

  @ClassRule
  public static ExternalResource webDriverProperties = new ExternalResource() {
    @Override
    protected void before() throws Throwable {
      baseUrl = PropertyLoader.loadProperty("site.url");
      capabilities = PropertyLoader.loadCapabilities();
    };
  };

  /**
   * Указание пути до драйвера
   */

  @Rule
  public ExternalResource webDriver = new ExternalResource() {
    @Override
    protected void before() throws Throwable {
      System.setProperty("webdriver.chrome.driver", "src/webDrivers/chromedriver.exe");
      driver = WebDriverPool.DEFAULT.getDriver(gridHubUrl, capabilities);
    };
  };
}
