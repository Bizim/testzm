package test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class PageWithMenu extends Page{

    protected WebDriver driver;
    private Page page;

    public PageWithMenu(WebDriver driver) {
        super(driver);
    }

    /**
     * Переход по пунктам меню
     * @param menuItem Название пункта меню, на который нужно перейти с главной страницы
     * @return Page Страница, на которую произошел переход при нажатии на пункт меню
     * @throws Exception
     */

    public Page goTo(String menuItem)throws Exception {
        if (canRedirect(menuItem)) {
            getFirstLevelMenu(menuItem).click();
            getSecondLevelMenu(menuItem).click();
            Page instanceOfClass = getRedirectClassInstance(menuItem);
            return instanceOfClass;
        }
        return this;
    }

    /**
     * Проверяется возможность перехода с текущей страницы по пункту меню
     * @param menuItem Название пункта меню, на который нужно перейти с главной страницы
     * @return Boolean
     * @throws Exception
     */

    public Boolean canRedirect(String menuItem) throws Exception {
        throw new Exception("It is abstract class");
    }

    /**
     * Получаем значение пункта меню
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return WebElement
     * @throws Exception
     */

    public WebElement getFirstLevelMenu(String menuItem)throws Exception {
        throw new Exception("It is abstract class");
    }

    /**
     * Получаем значение подпункта меню
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return WebElement
     * @throws Exception
     */

    public WebElement getSecondLevelMenu(String menuItem) throws Exception {
        throw new Exception("It is abstract class");
    }

    /**
     * Получаем объект следующей страницы
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return Page
     * @throws Exception
     */

    public Page getRedirectClassInstance(String menuItem) throws Exception {
        throw new Exception("It is abstract class");
    }
}
