package test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Элементы меню
 */

public class TranscriptCellPage extends Page  {

    public TranscriptCellPage (WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Кнопка 'Весь список'
     */

    @FindBy(xpath="/html/body/div[2]/font/input[4]")
    @CacheLookup
    public WebElement showAllList;

    /**
     * Строки таблицы с замечаниями
     */

    @FindBy(xpath = "/html/body/table[2]/tbody/tr")
    @CacheLookup
    public List<WebElement> transcriptRows;

}