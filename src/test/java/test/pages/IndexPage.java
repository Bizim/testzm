package test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Arrays;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexPage extends PageWithMenu {

    public IndexPage (WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "zm")
    public WebElement zmMenu;

    @FindBy(id = "analysis")
    public WebElement analysisMenu;

    @FindBy(id = "nsi")
    public WebElement nsiMenu;

    @FindBy(id = "admin")
    public WebElement adminMenu;

    @FindBy(id = "zm_in")
    public WebElement zmInSubMenu;

    @FindBy(id = "zm_in_ssps")
    public WebElement zmInSspsSubMenu;

    @FindBy(id = "zm_enter")
    public WebElement zmEnterSubMenu;

    @FindBy(id = "zm_enter_sl")
    public WebElement zmEnterSlSubMenu;

    @FindBy(id = "rb")
    public WebElement rbSubMenu;

    @FindBy(id = "out")
    public WebElement outSubMenu;

    @FindBy(id = "nbt")
    public WebElement nbtSubMenu;

    /**
     * Проверяется возможность перехода с текущей страницы по пункту меню
     * @param menuItem Название пункта меню, на который нужно перейти с главной страницы
     * @return Boolean
     * @throws Exception
     */

    public  Boolean canRedirect(String menuItem) {
        String[] redirectedPages = {"statisticOutZM", "statisticInZM"};
        if (Arrays.asList(redirectedPages).contains(menuItem)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Получаем значение пункта меню
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return WebElement
     * @throws Exception
     */

    public WebElement getFirstLevelMenu(String menuItem) throws Exception {
        WebElement firstLevelMenu;
        if ((menuItem == "statisticOutZM") || (menuItem == "statisticInZM")) {
            firstLevelMenu = zmMenu;
        }
        else {
            throw new Exception("Undefined menuItem state");
        }
        return firstLevelMenu;
    }

    /**
     * Получаем значение подпункта меню
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return WebElement
     * @throws Exception
     */

    public WebElement getSecondLevelMenu(String menuItem) throws Exception {
        WebElement secondLevelMenu;
        if (menuItem == "statisticOutZM") {
            secondLevelMenu = outSubMenu;
        }
        else if (menuItem == "statisticInZM") {
            secondLevelMenu = zmInSubMenu;
        }
        else {
           throw new Exception("Undefined menuItem state");
        }
        return secondLevelMenu;
    }

    /**
     * Получаем объект следующей страницы
     * @param menuItem Название пункта меню, на который нужно перейти
     * @return Page
     * @throws Exception
     */

    public Page getRedirectClassInstance(String menuItem) throws Exception {
        Page instanceClass;
        if ((menuItem == "statisticOutZM") || (menuItem == "statisticInZM")) {
            instanceClass = new StatisticPage(driver);
        }
        else {
            throw new Exception("Undefined menuItem state");
        }
       return instanceClass;
    }
}

