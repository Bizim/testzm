package test.pages;

        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;

        import org.openqa.selenium.support.CacheLookup;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.PageFactory;
        import test.pages.TranscriptCellPage;

        import java.util.List;

/**
 * Created by ivc_BizimovaEA on 14.09.2016.
 */
/**
 * Элементы меню
 */


public class StatisticPage extends Page {

    public StatisticPage (WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    /**
     * Ячейки таблицы на странице со статистикой исходящих замечаний
     */

    @FindBy(xpath = "/html/body/form/table[2]/tbody/tr/td[@class='stat']")
    @CacheLookup
    public List<WebElement> cellsShouldCheck;

    /**
     * Метод вызывается при нажатии на ячейку таблицы
     * @param xpath Xpath ячейки таблицы
     * @return TranscriptCellPage Страница со списком замечаний
     */
    public Page clickCell(String xpath) {
        clickByXpath(xpath);
        return new TranscriptCellPage(driver);
    }

}