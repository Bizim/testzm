package test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class Page {

  protected WebDriver driver;

  public Page(WebDriver driver) {
    this.driver = driver;
  }

  /**
   * Заполняем value для WebElement
   * @param element Элемент
   * @param text Текст
   */

  public void printText(WebElement element, String text) {
    element.clear();
    element.sendKeys(text);
  }

  /**
   * Click по элементу, используя xpath
   * @param xpath Xpath элемента
   */

  public void clickByXpath(String xpath) {
    WebElement button = driver.findElement(By.xpath(xpath));
    button.click();
  }

  /**
   * Инициируем возвращение в брауезере на 2 шага назад
   */

  public void twiceBack() {
    driver.navigate().back();
    driver.navigate().back();
  }
}
