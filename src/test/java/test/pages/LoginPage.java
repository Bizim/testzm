package test.pages;

import test.model.User;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import org.openqa.selenium.support.PageFactory;

public class LoginPage extends Page{

    private String url;

    /**
     * Логин
     */

    @FindBy(id = "login-username")
    public WebElement login;

    /**
     * Пароль
     */

    @FindBy(id = "login-password")
    public WebElement password;

    /**
     * Кнопка Вход
     */

    @FindBy(id = "btn-login")
    public WebElement btnLogin;

    public LoginPage(WebDriver webDriver, String baseURL) {
        super(webDriver);
        driver.navigate().to(baseURL);
        PageFactory.initElements(driver, this);
    }

    /**
     * Переход на главную страницу системы, путем ввода логина и пароля
     * @param user Объект пользователя, содержащий данные о логине и пароле
     * @return IndexPage
     */

    public IndexPage loginAs(User user) {
        printText(login, user.getLogin());
        printText(password, user.getPassword());
        btnLogin.click();
        return new IndexPage(driver);
    }
}
