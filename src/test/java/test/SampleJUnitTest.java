package test;
import java.util.HashMap;
import java.util.List;
import org.openqa.selenium.WebElement;

import test.formatter.CellsElementListFormatter;
import test.formatter.RowsElementListFormatter;
import test.pages.*;
import test.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.Map;
import test.extends_junit.AssertMap;

import org.openqa.selenium.support.PageFactory;

import static org.hamcrest.CoreMatchers.instanceOf;


import test.pages.IndexPage;

public class SampleJUnitTest extends JUnitTestBase {

    private StatisticPage statisticpage;
    private TranscriptCellPage transcriptcellpage;
    private IndexPage indexpage;

    /**
     * Инициализация объектов PageObject
     */

    @Before
    public void initPageObjects() {
        statisticpage      = PageFactory.initElements(driver, StatisticPage.class);
        transcriptcellpage = PageFactory.initElements(driver, TranscriptCellPage.class);
        indexpage          = PageFactory.initElements(driver, IndexPage.class);
    }

    /**
     * Тест, который проверяет, что в статистике исходящих замечаний каждое значение соответствует своей расшифровке
     */

    @Test
    public void testStatisticSendZM() throws Exception {
        LoginPage loginPage = new LoginPage(driver, baseUrl);
        PageWithMenu indexPage = loginPage.loginAs(User.ADMIN);

        Assert.assertThat(indexPage, instanceOf(IndexPage.class));

        Page statisticPage = indexpage.goTo("statisticOutZM");
        Assert.assertThat(statisticPage, instanceOf(StatisticPage.class));

        CellsElementListFormatter formatter = new CellsElementListFormatter(driver);

        List<WebElement> cellsShouldCheck = statisticpage.cellsShouldCheck;
        Map<String, String> formattedCells = formatter.getContent(cellsShouldCheck);

        Map<String, String> transcriptRowsLength = getCellTranscriptedArray(formattedCells);

        AssertMap.assertMap(formattedCells, transcriptRowsLength);
    }

    /**
     * Метод, возвращающий для каждой ячейки таблицы длину списка расшифровки
     *
     * @param formattedCells Список всех значений ячеек таблицы (ключ- xpath ячейки)
     */

    private Map<String, String> getCellTranscriptedArray(Map<String, String> formattedCells) {
        Iterator<Map.Entry<String, String>> entries = formattedCells.entrySet().iterator();
        Map<String, String> transcriptRowsLength = new HashMap<String, String>();

        while (entries.hasNext()) {
            Map.Entry<String, String> entry = entries.next();

            Page transcriptCellPage = statisticpage.clickCell(entry.getKey());
            Assert.assertThat(transcriptCellPage, instanceOf(TranscriptCellPage.class));

            transcriptcellpage = PageFactory.initElements(driver, TranscriptCellPage.class);
            transcriptcellpage.showAllList.click();

            List<WebElement> transcriptRows = transcriptcellpage.transcriptRows;

            RowsElementListFormatter formatter = new RowsElementListFormatter(driver);
            transcriptRowsLength.put(entry.getKey(), formatter.getLength(transcriptRows).toString().trim());

            transcriptCellPage.twiceBack();
        }
        return transcriptRowsLength;
    }
}
