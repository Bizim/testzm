package test.extends_junit;

import org.junit.Assert;
import java.util.*;

public class AssertMap extends Assert {

    /**
     * Assert для Map
     * @param expected Ожидаемый Map
     * @param actual Реальный Map
     */

    public static void assertMap(Map expected, Map actual) {
        Assert.assertEquals("length?", expected.size(), actual.size());
        Iterator entriesExpected = expected.entrySet().iterator();
        Iterator entriesActual = actual.entrySet().iterator();
        List<String> errorMap = new ArrayList<String>();

        while ((entriesExpected.hasNext()) && (entriesActual.hasNext())) {
            Map.Entry entryExpected = (Map.Entry) entriesExpected.next();
            Map.Entry entryActual = (Map.Entry) entriesActual.next();
            if (!(entryExpected.getValue().equals(entryActual.getValue()))) {
                errorMap.add("For xpath " + entryExpected.getKey().toString() + " Expected " + entryExpected.getValue() + " ,but actual: " + entryActual.getValue());
            }
        }
        if (errorMap.size() != 0) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < errorMap.size(); i++) {
                sb.append(errorMap.get(i));
                sb.append("\n");
            }
            fail(sb.toString());
        }
    }
}
